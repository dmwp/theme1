<?php 



get_header();

?>
    <div class="our-wines">
        <div class="winebottle">
            <img src="https://northwesternwine.oceancreativeserver.com/wp-content/uploads/sites/9/2019/08/Palacio.png"/>
            <h4>Wine Title</h4>
            <p>
                Barbera d'Alba DOC Valbianchèra
Our good friends from the Piedmonte region of northwestern Italy have several red and white wines to offer including this Barbera made from vines some 80 years old.  Grown on a mix of limestone, clay and sand at an altitude of 350m the grape juice spends 8-10 days on the skins and is aged 18 months in lightly toasted barrels.  We also have a very nice white Roero from these makers.
            </p>
        </div>
        <div class="winebottle">
            <img src="https://northwesternwine.oceancreativeserver.com/wp-content/uploads/sites/9/2019/08/Morey-Saint-Denis.png"/>
            <h4>Wine Title</h4>
            <p>
                From the southwest plateau of Sicily in the Agrigento region comes this beautiful white made from a combination of the indigenous Inzolia with some Chardonnay. 
The plateau is of marl with high clay and calcareous content.  Unusually, the grapes are harvested at different times - August for Chardonnay, mid September for Inzolia - and are fermented partly in stainless steel vats (90%) and a small portion (10%) in oak barrels. 
The Maria Costanza White - there is also a beautiful Nero d’Avola based red - was a milestone in Sicilian winemaking as the organic wine makers of Milazzo were the first to blend Inzolia and Chardonnay. 

            </p>
        </div>
    </div>

<div class="our-wines">
        <div class="winebottle">
            <img src="http://localhost/x3/wp-content/uploads/2019/08/Maria-Costanza-cropped.png"/>
            <h4>Wine Title</h4>
            <p>Wine text goes here</p>
        </div>
        <div class="winebottle">
            <img src="https://northwesternwine.oceancreativeserver.com/wp-content/uploads/sites/9/2019/08/Palacio.png"/>
            <h4>Wine Title</h4>
            <p>Wine text goes here</p>
        </div>
    </div>
    

<?php
get_footer();

?>